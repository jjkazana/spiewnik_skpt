# Depending on passed args:
# 1. Prepare songbook with all known songs, or
# 2. Prepare songbook with songs passed in cfg (JSON) file
# 3. List all available songs
#
#   Args:
#   make list_songs
#   make songs=songs.json

songs ?= None

pwd := $(shell pwd)

ifeq ($(songs), None)
# no config, take all songs
files_txt := $(wildcard Piosenki/*/*.txt)

else
# there's a config passed, leave filematch to list_songs.sh
files_txt := $(shell $(pwd)/list_songs.sh -c $(songs))

endif

files_tex := $(patsubst %.txt, LaTeX/%.tex, $(files_txt))

songs.tex: $(files_tex)
	cat $(sort $(files_tex)) > LaTeX/$@

LaTeX/%.tex: %.txt
	mkdir -p $(shell dirname $@)
	awk -F: -f prep_song.awk $< > $@

clean:
	rm -r LaTeX/Piosenki
	rm LaTeX/songs.tex
	rm LaTeX/main.aux
	rm LaTeX/main.log
	rm LaTeX/main.synctex.gz

list_songs:
	./list_songs.sh
