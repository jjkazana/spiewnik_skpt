# Spiewnik_SKPT

Kreator śpiewnika. Odpalenie zawartego Makefile'a złoży w katalogu "LaTeX" 
plik LaTeX'a zawierający kod śpiewnika.

## Co i jak?

# Obecnie głównie pod Linuxem - wymaga bash'a + awk'a + make'a
    sudo apt install awk make

# Pobranie (w konsoli):
    git clone https://gitlab.com/jjkazana/spiewnik_skpt.git

# Uruchomienie (konsola):
    
    Obecny Makefile pozwala na 3 osobne operacje:
    1. Stworzenie pliku "LaTeX/songs.tex" zawierającego teksty WSZYSTKICH piosenek,
    2. Wylistowanie dostępnych piosenek (po zapisaniu do pliku powstaje config)
    3. Złożenie pliku "LaTeX/songs.tex" z wybranymi piosenkami (na podstawie zrzuconego configa)

    W celu stworzenia pliku "songs.tex", zawierającego kod LaTeX'a ze wszystkimi p
    iosenkami, należy wykorzystać polecenie "make" bez dodatkowych argumentów:
    
    "make"

    W celu utworzenia pliku konfiguracyjnego, należy skopiować listę zwróconą
    po wywołaniu Makefile'a z argumentem "list_songs". Można zrzucic od razu do pliku:

    "make list_songs > songs_config.txt"

    Taki plik można następnie wyedytować (np. w notatniku), usuwając niechciane piosenki
    i podać w argumencie "songs" do Makefile'a (stworzenie śpiewnika z wybranymi piosenkami):

    "make songs=songs_config.txt"

    Następnie uruchom i skompiluj "main.tex" w katalogu "LaTeX" 
    (https://www.texstudio.org/). Sparsowane piosenki znajdują 
    się w pliku "LaTeX/songs.tex"

## Dodawanie nowych piosenek:
    Piosenki są ułożone wykonawcami w katalogu "Piosenki",
    każda zawarta w osobnym pliku
