BEGIN {
    title="";
    author="";
}

/^#/{
    if (/tytu[lł]/){title=$2}
    if (/autor/){author=$2}
}

END{
    print author, ": ", title
}
