# tytuł: Ssamotnemu żeglarzowi
# autor: EKT Gdynia

Toporem zręcznym, ostrzem nagłym  & e A e A
Wywiodę z lasu łódź i maszt,      & e D $H^7$ 
I spojrzę, jak u szczytu nieba    & e D C $H^7$
Wstępuje w obszar morza czas.     & e D A 

I przez samotność poprowadzę
Swych uniesionych powiek ból,
I będę wody odgadywał,
Gdzie świeci portów biała sól.

Ref: /x2
Oto są moje Himalaje,             & a $H^7$ e A
Pustynie i Arktyki lód.           & e D G $H^7$
To mój zgarnięty w jeden żagiel   & e D C $H^7$
Ludzkiej godności wieczny głód.   & e D A 

Zamknie się po mnie i zabliźni
Mój niedotknięty stopą ślad
I tylko w to do krwi uwierzę,
Co w dłoniach mi zawiąże wiatr.
