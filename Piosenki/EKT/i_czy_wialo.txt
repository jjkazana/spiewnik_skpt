# tytuł: I czy wiało
# autor: EKT Gdynia

Tap, tap, tap…. /x2                           & d g d A d g A d

Eh, żeglarskie życie, oto jest wyzwanie,     & d A d
Jak poczujesz wolę Bożą, pora na nie.        & d D g
Czas najwyższy, żeby zmierzyć się z falami,  & g d
A jeżeli w rejs, to tylko pod żaglami.       & g A d

Pomyślałem na początek dobry Bałtyk,
Gdyby nie przekleństwo permanentnej flauty.
A gdy wreszcie wiater raczył nieco zawiać,
To kapitan jakoś nie bardzo chciał żagle stawiać.

Ref:
I czy wiało, czy nie wiało,                  & g d
Na silniku jechaliśmy drogę całą,            & A d D 
Nasz kapitan wierzył w ludzi                 & g d 
I powtarzał: bez potrzeby mnie nie budzić!   & g A d 

Ryk maszyny nas katuje dniem i nocą,
A na masztach żadne żagle nie łopocą.
Wszystkie ryby ze zdziwienia oniemiały,
Bo z pokładu sterczą smutne, puste pały.

Gdynia miała nas powitać cichą keją,
A tu okazało się że wiatry w Gdyni akurat wieją.
Czyli, że co? W sumie się wyprawa oplacała,
Bo na koniec chociaż bandera łopotała.

Ref.: I czy wiało…

Hej Greenhorny, żółtodzioby, oto rada:
Ranga kapitana to taka raczej lekka jest przesada,
Bo jak w czasie rejsu mamy wiatru zanik,
To na łajbie najładniejszy się natychmiast robi kto? MECHANIK!

Ref.: I czy wiało…

Tap, tap, tap…
