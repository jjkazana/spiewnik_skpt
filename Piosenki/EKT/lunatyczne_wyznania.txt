# tytuł: Lunatyczna wyznania
# autor: EKT Gdynia

Idę, drogę mi oświetlają gwiazdy      & e a H7 e
A ludzie w domach śpią                & a H7 e
A ludzie w domach mrą                 & a $H^7$ e $E^7$

Ref.: A ja nie wiem, nie wiem po co     & a $D^7$
Lubię chodzić ciemną nocą               & G a
Ciemną nocą chodzić w ciemny las        & e $Fis^7 H^7$ e $(E^7)$

Zobacz jak ludzie lubią dzień
Jak boją się ciemności
Jak uciekają z mroku

Ref.: A ja nie wiem, nie wiem po co...

Zobacz, jak cudna toń jeziora
Jak księżyc złoty lśni
Jak złoty dukat lśni

Ref.: A ja nie wiem, nie wiem po co...

Hej Romane, hej Dziamare
Traderoma, hej Romane
Traderoma, traderoma hej
