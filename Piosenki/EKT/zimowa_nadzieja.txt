# tytuł: Zimowa nadzieja
# autor: EKT Gdynia \\ sł. I. Wójcicki, muz. J. Wydra

                                                    & C G a E F C G C
Dawno już nie było nas w naszych starych kątach     & C G F C
Pozarastał trawą leśny trakt                        & F C d G
Nawet błędny ognik zgasł, który nuty plątał         & C G F C
Blask którego drżał w piosenki takt                 & F C d G

Dawno nie widzieliśmy tego krańca świata            & F G E a 
Co za każdym razem inny był                         & F C D G
A tak bardzo chcieliśmy, żeby koniec lata           & C G F C 
Nie pozbawił do wędrówki sił, hej, hej              & F C D G

Ref.: Przeczekamy zimę, przeczekamy                 & C F G G
Nie pozbawi nas otuchy biały, zimny mróz            & F G E a
I na oszronioną szybę nachuchamy                    & F G E a
By zobaczyć, czy nie wraca lato już                 & F C D G
                                                    & C G a E F C G C
Jeszcze zapach letnich łąk z głowy nie wywietrzał
Jeszcze szum jeziora nam się śni
Jeszcze w ciepłej klatce rąk mały łyk powietrza
Zachowamy na zimowe dni

Noc i wieczór krótki dzień zatrzymują w biegu
Mróz na szybach swój rozłożył kram
Tylko wrony śmieją się brodząc w roziskrzonym śniegu
Kraczą, że nie starczy ciepła nam, hej, hej

Ref.: Przeczekamy zimę, przeczekamy...

(zmiana tonacji)
Ref.: Przeczekamy zimę, przeczekamy                 & D G A.
Nie pozbawi nas otuchy biały, zimny mróz            & G A Fish.
I na oszronioną szybę nachuchamy                    & G A Fis h.
By zobaczyć, czy nie wraca lato już                 & G A D DADD.

