# tytuł: Mary i John
# autor: EKT Gdynia 

Przybył z prerii do miasta żeby leczyć odciski      & C G
Mary stała przy barze, nalewała mu whisky           & G C
Spojrzał na nią i zadrżał, jej wypadło coś z rąk    & F C
No a w mieście rozległo się wkrąg:                  & D G $G^7$

Ref.: O... Mary i John      & C F C
Jak oni się kochają         & F C
Nie ma w całym Ohio         & G C
Pary jak Mary i John        & G C F

Gdy ją drugi raz ujrzał - schwytał konia na lasso
Mary stała przed szynkiem, jadła bułkę z kiełbasą
On powiedział "smacznego", ona jemu "oh sure"
I już wszyscy wiedzieli na mur

Ref.: O... Mary i John...
Johny musiał wyjechać, więc napoił mustanga
Jeszcze fajkę wykurzył i odśpiewał dwa tanga
Jeszcze wstąpił do szynku, jeszcze wypił swój rum
No a w mieście dopiero był szum

Ref.: O... Mary i John...

John wyruszył na prerię i nie myślał o Mary
Mary za mąż wnet wyszła i dziś córy ma cztery
Najdziwniejsze że im to nie przeszło przez myśl
Że w piosence się śpiewa do dziś

Ref.: O... Mary i John... x2
