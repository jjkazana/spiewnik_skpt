#!/usr/bin/env bash

with_filenames=0
cfg_file=0

while getopts f:c: flag
do
    case "${flag}" in
        c) cfg_file=${OPTARG};;
        f) with_filenames=1;;
    esac
done

if [[ $cfg_file != 0 ]]; then
    readarray -t cfg < "$(pwd)/$cfg_file"
    #echo "CFG: ${cfg[@]}"
fi

for song in $(find Piosenki/ | grep "\.txt"); 
do 
    data=$(awk -F: -f prep_config.awk $song)
    if [[ $with_filenames != 0 ]]; then
        # display path + song data
        echo "$song: $data"
    elif [[ $cfg_file != 0 ]]; then
        # config with songs was passed, display matching files only
        if [[ " ${cfg[@]} " =~ " $data " ]];then
            echo $song
        fi
    else
        # display only song data
        echo $data
    fi
done;
