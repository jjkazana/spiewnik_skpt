BEGIN{header_end=0}

/^#/{ 
    if (/tytu[lł]/)    {print "\\section*{\\center", $2, "}"}
    if (/autor/){print "\\flushright{", $2, "}"}
}

!/^#/{
    if (header_end == 0) {header_end = 1; print "\n\\begin{longtable}{p{10cm}p{5cm}}"}
    if (/&/) {print "\t", $0, "\\\\"} else {print "\t", $0, "&\\\\"}
}

END{print "\\end{longtable}\n"}
